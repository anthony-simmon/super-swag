module.exports = {
    Nothing: 0,
    Tree: 1,
    Player: 2,
    Tavern: 3,
    Spike: 4,
    MineP1: 5,
    MineP2: 6,
    MineP3: 7,
    MineP4: 8,
    MineUnclaimed: 9,
};
